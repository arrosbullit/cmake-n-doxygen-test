vim: tw=80
# What is this repo?

This repo is an example of using cmake to compile c files.

This repo is also an example of using doxygen to generate documentation.

This repo shows how to use Markdown links. Two kind of links actually:
links from Markdwon to Markdown and links from Markdown to HTML.

This repo shows how to create a table of contents in Markdown that is given
to doxygen to create an HTML table of contents.

A "doxygen document" is the result of running doxygen and it is actually not
a single file but a directory full of HTML files whose entry point is index.html.
A table of contents makes sense when one has several "doxygen documents" and one
needs a central point from where to access them all.

**Very important note**
I am only using cmake to compile the software but I am NOT using cmake to 
generate the doxygen doc! Instead, I am generating the doxygen doc directly.
Generating the doc with cmake can be complex and fortunately I found this other
repo that generated doxygen doc using camke. And in addition to that, it shows
how to generate doxygen documents by parts -that is doxygen documents that
include only a selection from all the source code directories:
https://github.com/yoyomatsu/DoxygenCmake.git

## Steps to compile the software using cmake
```
cd ./cmake-generated-stuff
cmake ..
make
```

Explanation: cmake generates a Makefile and a lot of other stuff. It is
healthier if all this stuff ends un in one separate directory: /cmake-generated-stuff

## Install PlantUML and inform doxygen about the installation path
Download and install PlantUML jar file: plantuml.jar

Then modify this line in files ./Doxyfile and ./Doxyfile-for-table-of-contents

```
PLANTUML_JAR_PATH      = /home/rob/downloaded-sw/plantuml/
```

## Steps to generate documentation using doxygen
To generate the documentation do:
```
doxygen ./Doxyfile
```
Doxyfile is the doxygen configuration file. The configuration states that the
output directory is html. Open html/index.html with a browser so see the documentation.

To generate the table of contents do:
```
doxygen ./Doxyfile-for-table-of-contents
```
Doxyfile-TOC  takes ./table-of-contents.md as input and generates
./html-table-of-contents as output. Open ./html-table-of-contents/index.html
with a browser to see it.

This TOC example is manually made. People who use cmake generate the TOC in a
more dynamic way by using complex cmake functions.

## Markdown link to another Markdown document

To make a link from one Markdown to another Markdown:

Mark destination (sample from child.md):
```
# My Child Documentation {#destination-id}
```

Create link to destination (sample from main.md) :
```
- [Child Markdown file](@ref destination-id)
```

## Table of contents example
```
ModuleListing {#moduleListing}
=======
This page contains only listing of modules documented
* [Main](../html/index.html)
* [Another link](../html/ug-child.html)
```

