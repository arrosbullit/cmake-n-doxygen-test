/*!
    @author robert
    @date 31-03-2020
    @brief My main() to test doxygen
    @see http://fnch.users.sourceforge.net/doxygen_c.html
    @see https://www.dummies.com/programming/cpp/c-creating-documentation-with-doxygen/
*/
#include "stdio.h"
#include "./swc-parent/parent.h"

/*!
    @brief main() function, starts it all. 
    
    But C lib init was done before I believe...
*/
int main(){
    printf("main...\n");
    runParent();
}
