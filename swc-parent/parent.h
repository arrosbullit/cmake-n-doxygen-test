/*!
    @brief runParent() prints "runParent..." and then calls runChild 
    
    runParent() has a dependency on runChild() which is declared
    in child.h
*/
void runParent();
