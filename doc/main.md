# My Main Documentation

This is the call sequence:

main.c:main() -- calls -> ../swc-parent/parent.c:runParent() -- calls --> ../swc-child/child.c:runChild()

Some other md files linked here:
- [Child Markdown file](@ref destination-id)
- [Child Future Work Link to section in Markdown](@ref ug-child-future-work)


PlantUML diagram:

@startuml
main.c -> parent.c: runParent()
parent.c -> child.c: runChild() 
child.c --> parent.c:
parent.c --> main.c
@enduml

