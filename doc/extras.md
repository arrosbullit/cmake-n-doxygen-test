\defgroup Extras Extra documents!
\brief Extra documents to try out other doxygen tags! 

Some tags to test:
* defgroup
* ingroup
* brief

See Markdown doc:
<http://www.doxygen.nl/manual/markdown.html>


